from __future__ import print_function
###########################################################################################################################
# Start here- run this (main file)- Has the global variables, audio, splash screen & data variables
###########################################################################################################################
from tkinter import *
from tkinter import filedialog as fd

import random
import math
import colorsys
import wave
import time
import sys
import numpy

import pyaudio
from numba import jit

#from PIL import Image, ImageTk, ImageGrab

import aubio
from aubio import source, pitch, notes

#import eyed3 # Song info (Meta data)

import os # for mp3 to wav conversion
from pydub import AudioSegment # for mp3 to wav conversion
 
# my own code files
from gui import *
from classicTheme import *
from spiralTheme import *

###########################################################################################################################
# setting up the global variables
###########################################################################################################################
flag = False
file = "" #./MusicFiles/wavTest"
theme = "classic" # default theme
beats = 0
pitch = 0
onsets = 0
onsets2 = 0
# for equalizer
levels = []
values = []

nLevels = 15 # num of levels in equalizer
SHORT_NORMALIZE = (1.0/32768.0)
        
###########################################################################################################################
# Set up audio and controls- plays file, converts mp3 to wav, threads music with visuals and normalizes the equalizer
###########################################################################################################################
class Player():
    def __init__(self):
        # fft size
        self.win_s = 1024
        # hop size
        self.hop_s = self.win_s // 2
        self.fileList = []
        self.filePlaying = False
        self.listStarted = False
        self.beats=0
        self.pitch=0
        self.loudness=0
        self.chunk=1024
        self.notes=0
        self.onsets=0
        self.onsets2=0
        self.onsetCount=0
        self.levels=[]
        self.values=[]
        self.samples=[]
        self.nLevels=15
        self.songIndex=0
        self.addWheel=False
        self.drop=False
        self.maxSample=0 # store to scale sample plotting properly
        self.incrementSpiral=False
        self.resetSpirals=False
        self.maxLoudness=0
        self.beatCount=0
        self.beatsList=[] 
        self.bpms=0.05
        self.lastBpms=60

    def initializeAudioList(self,file):
        # opens file as wave file
        self.src = file # + ".wav"
        samplerate = 0
        self.total_frames = 0
        
        # initialize aubio data
        self.a_source = aubio.source(self.src, samplerate, self.hop_s)
        self.samplerate = self.a_source.samplerate
        self.p = pyaudio.PyAudio()
        self.format = pyaudio.paFloat32
        self.frames = self.hop_s
        self.channels = 1
        self.p = pyaudio.PyAudio()
        
        self.a_tempo = aubio.tempo("default", self.win_s, self.hop_s, self.samplerate)
        self.pitch_o = aubio.pitch("yin", self.win_s, self.hop_s, self.samplerate)
        self.notes_o = aubio.notes("default", self.win_s, self.hop_s,self.samplerate)
        
        self.o = aubio.onset("default", self.win_s, self.hop_s, self.samplerate)
        self.o2 = aubio.onset("hfc", self.win_s, self.hop_s, self.samplerate)
        
        print("Audio set up for", file)

    def buttons(self, frame,data):
        global file, flag
        #self.playButton = Button(frame, text = "play", command = self.play)
        #self.playButton.place(anchor="nw",x=5,y=45)

        #self.fileButton = Button(frame, text = "Select song", command = self.openFile)
        #self.fileButton.place(anchor="nw",x=5,y=5)
    
        self.fileListButton = Button(frame, text = "Select directory", command = self.openDirectory)
        self.fileListButton.place(anchor="se",x=data.width,y=data.height*0.85)
        
        self.startListButton = Button(frame, text = "Start list", command = self.startList)
        self.startListButton.place(anchor="se",x=data.width,y=data.height*0.9)
    
        self.pauseListButton = Button(frame,text="Pause/Play song",command=self.playFile)
        self.pauseListButton.place(anchor="se",x=data.width,y=data.height*0.95)
    
        self.nextListButton = Button(frame,text="Next song",command=self.nextSong)
        self.nextListButton.place(anchor="se",x=data.width,y=data.height)
    
#        self.nextListButton = Button(frame,text="Add Wheel",command=self.wheel)
#        self.nextListButton.place(anchor="se",x=data.width,y=data.height)

    def wheel(self):
        self.addWheel = not self.addWheel
        print("wheel is now", self.addWheel)

    def openDirectory(self):
        dirName = fd.askdirectory()
        print("found dir named", dirName)
        #if dirName != None:
        files = os.listdir(dirName)
        for file in files:
            if file.startswith('.'): continue
            #print("looking for file", file)
            temp = file.split('.')
            #print("temp",temp[0],temp[1])
            mp3File = dirName+'/'+temp[0] + ".mp3"
            wavFile = dirName+'/'+temp[0] + ".wav"
            if not os.path.isfile(wavFile):
                AudioSegment.converter = "/usr/local/bin/ffmpeg"
                sound = AudioSegment.from_mp3(mp3File)
                sound.export(wavFile, format = "wav")
                self.fileList.append(wavFile)
            if temp[1] != "mp3":
                self.fileList.append(wavFile)
                print(wavFile, "added to playlist")

    def setTheme(self):
        global theme
        if theme == "classic":
            theme = "fun"
        elif theme == "fun":
            theme = "classic"

    def startList(self):
        if self.filePlaying==False:
            self.initializeAudioList(self.fileList[self.songIndex])
            print("now playing",file)
            self.playFile()

    def nextSong(self):
        if not self.filePlaying: return
        self.stream.stop_stream()
        self.stream.close()
        self.p.terminate()
        self.filePlaying=False
        self.resetSpirals=True
        self.maxSample=0
        self.maxLoudness=0
        self.incrementSpiral=False
        self.songIndex+=1
        self.startList()

    def playFile(self):
        if not self.filePlaying:
                self.filePlaying=True
                self.stream = self.p.open(format = self.format, channels = self.channels, rate = self.samplerate, input = False, output = True, frames_per_buffer = self.frames, stream_callback = self.pyaudio_callback)
                self.stream.start_stream()
        else:
                self.filePlaying=False
                self.stream.stop_stream()

    # pyaudio callback- my version of threading (non-blocking)
    # the def and return statement : 
        # cited from [https://people.csail.mit.edu/hubert/pyaudio/docs/#example-callback-mode-audio-i-o] 
    # all intermediary code is written by me.
    def pyaudio_callback(self, _in_data, _frame_count, _time_info, _status):
        self.samples, read = self.a_source()
        is_beat = self.a_tempo(self.samples)
        self.pitch = self.pitch_o(self.samples)[0]
        #block = read(self.chunk)
        self.loudness = self.get_rms(self.samples)
#        if self.checkLoudness:
#            self.maxLoudness = self.loudness
#            self.incrementSpiral = not self.incrementSpiral
        #print("loudness", self.loudness)
        self.notes = self.notes_o(self.samples)

        if is_beat:
            self.beats = 1
            this_beat = self.o.get_last_s()
            self.beatsList.append(this_beat)
        else: 
            self.beats = 0
        if self.o(self.samples):
            self.onsets = 1
        else: 
            self.onsets = 0
        if self.o2(self.samples):
            onsets2 = 1
        else:
            onsets2 = 0
        # appends pitch values to list
        self.levels.append(self.pitch)
        avg = 1
        # removes oldest pitch value if length exceeds desired num of levels
        if len(self.levels) > self.nLevels:
           self.levels.remove(self.levels[0])
        if len(self.levels) >= self.nLevels:
            avg = sum(self.levels)/len(self.levels)
        self.values = []
        for i in range(len(self.levels)):
            self.values.append(min(50, 15 * self.levels[i] / avg))
            #print("levels i",self.levels[i])
        
        #print("len of values",len(self.values))

        audiobuf = self.samples.tobytes()
        if read < self.hop_s:
            return (audiobuf, pyaudio.paComplete)
        return (audiobuf, pyaudio.paContinue)

    def get_rms(self,samples):
        count = len(samples)/2
        format = "%dh" % (count)
        #shorts = struct.unpack( format, block )
        sum_squares = 0.0
        for sample in samples:
            n = sample * SHORT_NORMALIZE
            sum_squares += n*n
        return math.sqrt( sum_squares / count )

# Give Hex colors based on pitch radius and loudness

def getColorFromRadiusSpiral(radius,loudness):
        #print("spiral radius used",radius)
        factor = (radius/216)
        factor2 = abs(loudness/1e-5)*0.5
        if factor>1.0: factor=0.5
        if factor2>1.0: factor2=1.0
        rgb = colorsys.hsv_to_rgb(factor,0.8,factor2)
        str = '#%02x%02x%02x' % (int(rgb[0]*255), int(rgb[1]*255), int(rgb[2]*255))
        return str

def getColorFromPitch(pitch,loudness):
        factor = (pitch/375)
        factor2 = abs(loudness/1e-5)
        if factor>1.0: factor=1.0
        if factor2>1.0: factor2=1.0
        rgb = colorsys.hsv_to_rgb(factor,0.8,factor2)
        str = '#%02x%02x%02x' % (int(rgb[0]*255), int(rgb[1]*255), int(rgb[2]*255))
        return str

def getColorFromPitchFaded(pitch,loudness):
        factor = abs(pitch/375)
        factor2 = abs(loudness/1e-5)*0.5
        if factor>1.0: factor=1.0
        if factor2>1.0: factor2=0.5
        #print("factor",factor)
        rgb = colorsys.hsv_to_rgb(factor,0.8,factor2)
        str = '#%02x%02x%02x' % (int(rgb[0]*255), int(rgb[1]*255), int(rgb[2]*255))
        return str

def getColorFromAge(oldColor, age):
        str = oldColor.split('#')
        #print("age coming in",age)
        int1,int2,int3 = int(str[1][0:2],16),int(str[1][2:4],16),int(str[1][4:],16)
        hsv = colorsys.rgb_to_hsv(int1/255,int2/255,int3/255)
        factor3 = age*1.0
        if factor3>1: factor3=1
        rgb = colorsys.hsv_to_rgb(hsv[0],hsv[1],hsv[2]*factor3)
        #print(int1,int2,int3)

        str = '#%02x%02x%02x' % (int(rgb[0]*255), int(rgb[1]*255), int(rgb[2]*255))
        return str

###########################################################################################################################
# Initializing data variables
###########################################################################################################################


def init(data):
    data.wheel = []
    data.dots = []
    data.waves = []
    data.logo = []
    data.spiral = []
    data.spiral2 = []
    data.spiral3 = []
    data.spiral4 = []
    data.spiral5 = []
    data.spiral6 = []
    data.spiral7 = []
    data.spiral8 = []
    data.sine = []
    data.color = []
    data.lines=[]
    
    rgbTuples = []
    for i in range(255):
        rgb = colorsys.hsv_to_rgb(i/255,1.0,1.0)
        str = '#%02x%02x%02x' % (int(rgb[0]*255), int(rgb[1]*255), int(rgb[2]*255))
        data.color.append( str )
    
    #data.color = ["#5DA5DA", "#FAA43A", "#60BD68", "#D32F2F", "#F15854", "#FF4081", "#8E24AA", "#7C4DFF", "#F17CB0","#3D5AFE", "#4DD0E1", "#B2FF59", "#FF6F00"]
    if theme == "classic":
         data.fill = "#3D5AFE" #data.color[random.randint(0,len(data.color) - 1)]
    else:
         data.fill = "#D32F2F"

    data.time = 0
    data.increaseSpiralR=True
    data.timeLastLoudCheck = -5000
    data.text = "play"
    # centre of the canvas
    data.centreX = (data.width / 2)
    data.centreY = (data.height / 2)

    # omega for wheel
    data.omegaW = 40
    # omega for logo
    data.omegaL = 40
    # omega for spiral
    data.omegaS = 3

    data.reverseAngleS=-1 # control direction of dot spin
    # counter for wheel
    data.counter = 0
    # counter for logo
    data.counterL = 0

    # Wheel
    data.wheelR = 1 # 0
    data.rWheel = 1 #0
    # Dots
    data.dotsR = 6

    # Waves
    data.wavesR = 50
    data.wavesVel = 5

    # Logo circles
    data.logoR = 40
    data.rLogo = 8

    # Spiral
    data.spiralR = 100
    data.rSpiral = 5
    data.angleS = 0

    data.dt = 0.05
    data.anglePitchDots=0
    # Splash screen
    data.splashR = data.logoR + 20
    data.rSplash = 4
    data.omegaSplash = 1
    data.splash = []
    data.angleSplash = 0
    data.mSplash = 30

def getEllipse(radius, x, y, factor, data):
    x0 = x - radius
    y0 = y + radius * factor
    x1 = x + radius
    y1 = y - radius * factor
    return (x0, y0, x1, y1)

def getCircle(radius, x, y, data):
    x0 = x - radius
    y0 = y + radius
    x1 = x + radius
    y1 = y - radius
    return (x0, y0, x1, y1)

def getMeta(canvas, data):
    global musicPlayer
    
    if musicPlayer.filePlaying:
        songText = musicPlayer.fileList[musicPlayer.songIndex]
        #print("song text",songText)
        str = songText.split('/')
        str = str[-1].split('.')
        canvas.create_text(data.width*0.6, data.height*0.95, fill = data.fill, justify = "right", text = 'Now Playing \n' + str[0], font = "Garamond 15 bold")

def drawEqualizer(canvas, data):
    global theme, musicPlayer
    if theme == "fun":
        fill = "black"
    elif theme == "classic":
        fill = "white"
    x0 = 10
    width = data.width/35
    # gap between rectangles (levels)
    sep = 2
    space = sep + width
#    if len(musicPlayer.values) >= musicPlayer.nLevels:
#        for i in range(nLevels - 1):
#            if theme == "fun":
#                i = random.randint(0,len(data.color) - 1)
#                fill = data.color[i]
#            else:
#                fill = data.fill
#            canvas.create_rectangle(x0 + i * space, data.height - musicPlayer.values[i], x0 + i * space + width, data.height, width = 1, outline = fill, fill = fill)


def redrawAll(canvas, data):
    global theme, musicPlayer
    rand1 = random.uniform(-1,1)
    rand2 = random.uniform(-1,1)

    if not musicPlayer.filePlaying:
        # Splash page animation
        for p in data.splash:
            p.drawSplash(canvas, data)

    # the following things are drawn only when music plays
    if musicPlayer.filePlaying:
        data.centreY += rand1
        data.centreX += rand2
        drawEqualizer(canvas, data)
#        if theme == "classic":
#            # draw waves
#            for p in data.waves:
#                p.drawWaves(canvas,data,getColorFromRadius2(p.radius/100))

        maxSample = plotSamples(canvas,musicPlayer.samples,data,musicPlayer.maxSample)
        musicPlayer.maxSample = maxSample
        if theme == "classic":
            # draw spirals
            for p in data.spiral:
                p.drawSpiral(canvas, data)
            for p in data.spiral2:
                p.drawSpiral(canvas, data)
            for p in data.spiral3:
                p.drawSpiral(canvas, data)
            for p in data.spiral4:
                p.drawSpiral(canvas, data)
            if musicPlayer.incrementSpiral:
                for p in data.spiral5:
                    p.drawSpiral(canvas, data)
                for p in data.spiral6:
                    p.drawSpiral(canvas, data)
                for p in data.spiral7:
                    p.drawSpiral(canvas, data)
                for p in data.spiral8:
                    p.drawSpiral(canvas, data)

        # draw lines
        for p in data.lines:
            p.drawLines(canvas,data,data.color[200],musicPlayer.loudness)

        # draw circle of wheel
        (x0, y0, x1, y1) = getCircle(musicPlayer.loudness*5e5, data.centreX, data.centreY, data)
        # draws orbit that is the ring connects the circles

        canvas.create_oval(x0, y0, x1, y1, outline = getColorFromPitch(musicPlayer.pitch,musicPlayer.loudness), width = musicPlayer.loudness*9e5)

        
        # draw pitch dots emenating from center
        for p in data.dots:
            color = getColorFromPitchFaded(musicPlayer.pitch,musicPlayer.loudness)
            p.drawDots(canvas, data,color)

        # draw wheel
        if musicPlayer.addWheel:
            for p in data.wheel:
                colorByPitch = getColorFromPitch(musicPlayer.pitch,musicPlayer.loudness)
                p.drawWheel(canvas, data, colorByPitch)

    # draw logo circles
    for p in data.logo:
        p.drawLogo(canvas, data,getColorFromPitch(musicPlayer.pitch,musicPlayer.loudness))

    logo1 = canvas.create_text(data.width*0.82, data.height*0.95, text = "VisualEyes", fill = data.color[100],font = "Didot 30 bold" )
    # displays meta data on screen (song info)
    getMeta(canvas, data)


def timerFired(data):
    global musicPlayer
    data.time += data.timerDelay
    
    if musicPlayer.filePlaying:
        # setting baseline values

 #musicPlayer.loudness*2e5#min(data.height // 2, musicPlayer.pitch / 3 + data.logoR + 20)
        data.rWheel = 10

        nWheel = 30
        numDots = 100#musicPlayer.pitch
        nDots = 5
        nWaves = 5
        numLines = 0

        data.omegaW = musicPlayer.pitch / 40
        data.omegaL = musicPlayer.pitch / 20

        # for beats
        if musicPlayer.beats > 0:
            #data.rWheel = musicPlayer.loudness #15
            numDots = 200# musicPlayer.pitch * 5
            data.omegaW = musicPlayer.pitch / 20
            nDots = 5
            nWheel = 20
            #data.anglePitchDots+=musicPlayer.loudness #21

        # for onsets 
        if musicPlayer.onsets > 0:
            numDots = 150# musicPlayer.pitch * 5
            nDots = 5
            data.fill = getColorFromPitch(musicPlayer.pitch,musicPlayer.loudness) #data.color[i]
            #print("using notes",musicPlayer.notes)
            #data.rWheel = musicPlayer.loudness #15
            #data.anglePitchDots-=musicPlayer.pitch #32

        # Wheel
#        if musicPlayer.addWheel:
#            if data.counter < (1 + 360/nWheel):
#                for angle in range(0, 360, nWheel):
#                    theta = (angle / 180) * math.pi
#                    data.wheel.append(Wheel(data.rWheel, theta, data)) # data.wheel.append(Wheel(data.rWheel, theta, data))
#                    data.counter += 1

#            for p in data.wheel:
#                p.onTimerFired(data)
#        else: data.wheel = []

        # Generates Dots
        #print("angles", np.linspace(0,360,nDots))
        for angle in np.linspace(0,360, 4,endpoint=False): #nDots):
            data.anglePitchDots+=musicPlayer.pitch*musicPlayer.loudness*1e2
            theta = ((data.anglePitchDots +angle) / 180) * math.pi
            data.dots.append(Dots(musicPlayer.pitch/30, theta)) # data.dots.append(Dots(data.dotsR, theta))
        if len(data.dots) > numDots:
            data.dots = data.dots[numDots:]

        for p in data.dots:
            p.onTimerFired(data)

#        # Generates waves
#        if musicPlayer.onsets > 0:
#            data.wavesVel = -5
#        k = int(data.wheelR)
#        for r in range(0, k, 10):
#            data.waves.append(Waves(r))
#        if len(data.waves) > nWaves:
#            data.waves = []

#        for p in data.waves:
#            p.onTimerFired(data)

#        # generate lines
#        for i in range(numLines):
#            data.lines.append(Lines())

        # Generate spiral
        if musicPlayer.filePlaying:
            if data.spiralR>data.height*0.2:data.increaseSpiralR=False
            elif data.spiralR<data.height*0.05:data.increaseSpiralR=True
            if data.increaseSpiralR: data.spiralR+=1
            else: data.spiralR-=1
             # min(data.height // 5, musicPlayer.pitch)
            numSpiral = 150#musicPlayer.pitch*2 #musicPlayer.loudness * 5e6
            #data.angleS += 3

            # beats
            if any(np.diff(musicPlayer.beatsList))==0.: 
                musicPlayer.bpms = musicPlayer.lastBpms
            else:
                musicPlayer.bpms = np.mean(60./np.diff(musicPlayer.beatsList))
                if musicPlayer.bpms < 0.1 or np.isinf(musicPlayer.bpms): musicPlayer.bpms=musicPlayer.lastBpms
            musicPlayer.lastBpms = musicPlayer.bpms
            #data.angleS += musicPlayer.bpms
            beatAngleFactor = 45 * musicPlayer.bpms/300
            if data.angleS+musicPlayer.bpms>360: data.angleS=(data.angleS+data.reverseAngleS* beatAngleFactor) - 360
            else: data.angleS+= data.reverseAngleS* beatAngleFactor
            #print("angle and bpms",data.angleS, musicPlayer.bpms)
            #data.spiralR = musicPlayer.bpms
                #print("diff",np.diff(musicPlayer.beatsList))
            if musicPlayer.beats > 0:
                musicPlayer.beatCount+=1
                if musicPlayer.beatCount>2:
                    #print("reverse spin")
                    musicPlayer.beatCount=0
                    data.omegaS =-musicPlayer.bpms/70 #-(musicPlayer.pitch/300) #musicPlayer.bpms/100 #-9
                    data.reverseAngleS=-data.reverseAngleS## reverse direction of the wheel?
                    #if data.time - data.timeLastLoudCheck > 20:
                    #    data.timeLastLoudCheck=data.time
                    #	
                    # #musicPlayer.bpms/50 #3
                    #data.spiralR = musicPlayer.pitch  #min(data.height // 5, musicPlayer.pitch)
                    #numSpiral = 200 #musicPlayer.pitch*6
                    data.dt = musicPlayer.bpms/2000#0.05
                    musicPlayer.beatsList = []

            # onsets
            if musicPlayer.onsets > 0:
                musicPlayer.onsetCount+=1
                if musicPlayer.onsetCount>2:
                    musicPlayer.onsetCount=0
                    data.omegaS = musicPlayer.bpms/30
                    data.reverseAngleS=-data.reverseAngleS
                    #data.spiralR = musicPlayer.loudness*1e7
                    #data.spiralR = musicPlayer.pitch #min(data.height // 5, musicPlayer.pitch)
                    #numSpiral = 200 #musicPlayer.pitch*5
                    data.dt = musicPlayer.bpms/2000 #0.01 #60./np.diff(musicPlayer.beatsList) #0.01
                    musicPlayer.beatsList = []

            radius = musicPlayer.loudness*5e5
            theta = (data.angleS / 180) * math.pi
            theta2 = ((data.angleS + 90) / 180) * math.pi
            theta3 = ((data.angleS + 180) / 180) * math.pi
            theta4 = ((data.angleS + 270) / 180) * math.pi
            theta5 = ((data.angleS + 225) / 180) * math.pi
            theta6 = ((data.angleS + 135) / 180) * math.pi
            theta7 = ((data.angleS + 315) / 180) * math.pi
            theta8 = ((data.angleS + 45) / 180) * math.pi

            
            #colorByRadius2 = getColorFromRadiusSpiral(data.spiralR,musicPlayer.loudness)
            colorByRadius2 = getColorFromPitch(musicPlayer.pitch,musicPlayer.loudness)
           
            data.spiral.append(Spiral(theta, radius, colorByRadius2, data))  # make color function of radius?
            data.spiral3.append(Spiral(theta3, radius, colorByRadius2, data))
            
            
            data.spiral2.append(Spiral(theta2, radius, colorByRadius2, data))
            data.spiral4.append(Spiral(theta4, radius, colorByRadius2, data))

            if musicPlayer.incrementSpiral:
                #print("beat dropped?")
                data.spiral5.append(Spiral(theta5,radius*0.2,colorByRadius2,data))
                data.spiral6.append(Spiral(theta6,radius*0.2,colorByRadius2,data))
                data.spiral7.append(Spiral(theta7,radius*0.2,colorByRadius2,data))
                data.spiral8.append(Spiral(theta8,radius*0.2,colorByRadius2,data))

#            if data.angleS >= 360:
#                data.angleS = 0

            if len(data.spiral) > numSpiral or musicPlayer.resetSpirals:
                #print('truncating spiral')
                if musicPlayer.resetSpirals:
                    musicPlayer.resetSpirals = False
                    numSpiral=0
                data.spiral = data.spiral[int(numSpiral/2.):]
                data.spiral2 = data.spiral2[int(numSpiral/2.):]
                data.spiral3= data.spiral3[int(numSpiral/2.):]
                data.spiral4 = data.spiral4[int(numSpiral/2.):]
                data.spiral5= data.spiral5[int(numSpiral/2.):]
                data.spiral6 = data.spiral6[int(numSpiral/2.):]
                data.spiral7= data.spiral7[int(numSpiral/2.):]
                data.spiral8 = data.spiral8[int(numSpiral/2.):]

            i=0
            for p in data.spiral:
                i+=1
                str = getColorFromAge(p.fill,i/len(data.spiral)*2)
                p.onTimerFired(data,str)
            i=0
            for p in data.spiral2:
                i+=1
                str = getColorFromAge(p.fill,i/len(data.spiral)*2)
                p.onTimerFired(data,str)
            i=0
            for p in data.spiral3:
                i+=1
                str = getColorFromAge(p.fill,i/len(data.spiral)*2)
                p.onTimerFired(data,str)
            i=0
            for p in data.spiral4:
                i+=1
                str = getColorFromAge(p.fill,i/len(data.spiral)*2)
                p.onTimerFired(data,str)
            i=0
            for p in data.spiral5:
                i+=1
                str = getColorFromAge(p.fill,i/len(data.spiral))
                p.onTimerFired(data,str)
            i=0
            for p in data.spiral6:
                i+=1
                str = getColorFromAge(p.fill,i/len(data.spiral))
                p.onTimerFired(data,str)
            i=0
            for p in data.spiral7:
                i+=1
                str = getColorFromAge(p.fill,i/len(data.spiral))
                p.onTimerFired(data,str)
            i=0
            for p in data.spiral8:
                i+=1
                str = getColorFromAge(p.fill,i/len(data.spiral))
                p.onTimerFired(data,str)

    # Generate logo
    if data.counterL < 8:
        for angle in np.linspace(0, 360, 6):
            theta = (angle / 180) * math.pi
            data.logo.append(Logo(theta, data))
            data.counterL += 1

    for p in data.logo:
        p.onTimerFired(data)

    # Generate Splash page spiral
    if not musicPlayer.filePlaying:
        i = random.randint(0, len(data.color) - 1)
        fill = data.color[i]  # MAKE AS FUNCTION OF RADIUS
        numSplash = 200
        data.rSplash = 0
        data.angleSplash += data.mSplash
        data.rSplash = 1
        thetaSplash = (data.angleSplash / 180) * math.pi
        data.splash.append(Splash(thetaSplash, data.rSplash, fill))
        if data.angleSplash >= 360:
            data.angleSplash = 0
        # change spiral type each time data.splash is reset
        # changing data.mSplash changes type of spiral
        if len(data.splash) > numSplash:
            if data.mSplash == 15:
                data.mSplash = 30
            else:
                data.mSplash = 15
            data.splash = []
        for p in data.splash:
            p.onTimerFired(data)

###########################################################################################################################
# run function
###########################################################################################################################
def run(width=300, height=300):
    def redrawAllWrapper(canvas, data):
        global theme
        if theme == "classic":
            fill = "black"
        elif theme == "fun":
            fill = "white"

        canvas.delete(ALL)

        canvas.create_rectangle(0, 0, data.width, data.height, fill = fill, outline = fill, width = 0)
        redrawAll(canvas, data)
        canvas.update()

    def mousePressedWrapper(event, canvas, data):
        mousePressed(event, data)
        redrawAllWrapper(canvas, data)

    def keyPressedWrapper(event, canvas, data):
        keyPressed(event, data)
        redrawAllWrapper(canvas, data)

    def timerFiredWrapper(canvas, data):
        timerFired(data)
        if musicPlayer.filePlaying:
            if not musicPlayer.stream.is_active():
                musicPlayer.stream.stop_stream()
                musicPlayer.stream.close()
                musicPlayer.p.terminate()
                musicPlayer.maxLoudness=0
                musicPlayer.incrementSpiral=False
                musicPlayer.filePlaying=False
                musicPlayer.beats=0
                musicPlayer.pitch=0
                musicPlayer.onsets=0
                musicPlayer.onsetCount=0
                musicPlayer.onsets2=0
                musicPlayer.levels=[]
                musicPlayer.values=[]
                musicPlayer.maxSample=0
                musicPlayer.songIndex+=1
                musicPlayer.resetSpirals=True
                musicPlayer.startList()
        #canvas = Canvas(root, width = data.width, height = data.height)
        redrawAllWrapper(canvas, data)
        # pause, then call timerFired again
        canvas.after(data.timerDelay, timerFiredWrapper, canvas, data)

    # Set up data and call init
    class Struct(object): 
        pass
    data = Struct()
    data.width = width
    data.height = height
    
    data.timerDelay = 1 # milliseconds
    
    init(data)
    # create the root and the canvas
    root = Tk()
    canvas = Canvas(root, width = data.width, height = data.height)
    
    canvas.pack() #side=TOP)
    
    
    # and launch the app
    #root.mainloop()  # blocks until window is closed
    GUI(root)
    global musicPlayer
    musicPlayer = Player()
    musicPlayer.buttons(root,data)
    timerFiredWrapper(canvas, data)
    #app = Player().openFile() #buttons(root) #(root)
    #Player().play()
    root.mainloop()
    print("bye!")
    
run(1250,750)
