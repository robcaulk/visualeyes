###########################################################################################################################
# Visuals- Draws the "Classic" theme visuals (Wheel, Dots, Waves)
###########################################################################################################################
from tkinter import *
from tkinter import filedialog as fd

import random
import math
import numpy as np

import colorsys

import time
import sys

###########################################################################################################################
# setting up the global variables
###########################################################################################################################
flag = False
file = "" # default file
theme = "classic" # default theme
beats = 0
pitch = 0
onsets = 0
onsets2 = 0
# for equalizer
levels = []
values = []
nLevels = 15 # num of levels in equalizer

def getColorFromSample(sample):
        factor = 1-abs(sample)
        rgb = colorsys.hsv_to_rgb(factor,1.0,1.0)
        str = '#%02x%02x%02x' % (int(rgb[0]*255), int(rgb[1]*255), int(rgb[2]*255))
        return str

def plotSamples(canvas,samples,data,maxSample):
    #print("samples looks like",samples)
    
    if len(samples)==0: return maxSample
    if max(samples) > maxSample: maxSample = max(samples)
    if maxSample==0: return maxSample
    #samples = np.asarray(samples)
    xPoints = np.linspace(0,data.width,len(samples))
    
    yFactor = data.height*0.9/maxSample #np.max(samples)
    for i in range(0,len(samples)):
        colorStr = getColorFromSample(samples[i])
        (x0, y0, x1, y1) = getCircle(1, xPoints[i], abs(samples[i])*yFactor+data.centreY*0.2, data)
        dots = canvas.create_oval(x0, y0, x1, y1, fill = colorStr)
    return maxSample

        
###########################################################################################################################
# creates Animations
###########################################################################################################################
def getCircle(radius, x, y, data):
    x0 = x - radius
    y0 = y + radius
    x1 = x + radius
    y1 = y - radius
    return (x0, y0, x1, y1)

###########################################################################################################################
# Wheel with circles- circles in an orbit connected by a ring
###########################################################################################################################
class Wheel(object):
    def __init__(self, radius, theta, data):
        self.radius = radius
        self.theta = theta
        
    def drawWheel(self, canvas, data, color):
        x = data.centreX + data.wheelR * math.cos(self.theta)
        y = data.centreY + data.wheelR * math.sin(self.theta)
        if theme == "fun":
            fill = color
        else:
            fill = color
        (x0, y0, x1, y1) = getCircle(self.radius, x, y, data)
        canvas.create_oval(x0, y0, x1, y1, fill = fill, outline = fill, width = 0)

    def onTimerFired(self, data):
        dt = 0.001
        dtheta = data.omegaW * dt
        self.theta = self.theta + dtheta

###########################################################################################################################
# Dots- circles emanating from the centre
###########################################################################################################################
class Dots(object):
    def __init__(self, radius, theta):
        self.centreX = 0
        self.centreY = 0
        self.radius = radius
        self.theta = theta
        self.gap = 15

    def drawDots(self, canvas, data,color):
        (x0, y0, x1, y1) = getCircle(self.radius, data.centreX + self. centreX, data.centreY + self.centreY, data)
        dots = canvas.create_oval(x0, y0, x1, y1, outline = color)

    def onTimerFired(self, data):
        self.centreX += math.cos(self.theta) * self.gap
        self.centreY += math.sin(self.theta) * self.gap
        self.radius += 0.25

###########################################################################################################################
# Waves- concentric circles emanating from the center
###########################################################################################################################
class Waves(object):
    def __init__(self, radius):
        self.radius = radius

    def drawWaves(self, canvas, data,color):
        (x0, y0, x1, y1) = getCircle(self.radius, data.width/2, data.height/2, data)
        waves = canvas.create_oval(x0, y0, x1, y1, width = 1, outline = color)

    def onTimerFired(self, data):
        self.radius -= 2 * data.wavesVel

class Lines(object):
    def __init__(self): #,width):
        self.width=0
    
    def drawLines(self,canvas,data,color,loudness):
        lines = canvas.create_line(0, data.centreY+loudness*1e6, data.width, data.centreY-loudness*1e6, fill = color, width = loudness*1e6, dash = (6,5,2,4))

class Logo(object):
    def __init__(self, theta, data):
        self.theta = theta

    def drawLogo(self, canvas, data,color):
        global flag
        x = data.centreX + data.logoR * math.cos(self.theta)
        y = data.centreY + data.logoR * math.sin(self.theta)
        fill = color
        (x0, y0, x1, y1) = getCircle(data.rLogo, x, y, data)
        canvas.create_oval(x0, y0, x1, y1, fill = fill, width = 0)

    def onTimerFired(self, data):
        dt = 0.001
        dtheta = data.omegaL * dt
        self.theta = self.theta + dtheta

class Splash(object):
    def __init__(self, theta, radius, fill):
        self.theta = theta
        self.radius = radius
        self.dx = 0
        self.dy = 0
        self.fill = fill
	

    def drawSplash(self, canvas, data):
        x = data.centreX + data.splashR * math.cos(self.theta) + self.dx
        y = data.centreY + data.splashR * math.sin(self.theta) + self.dy
        (x0, y0, x1, y1) = getCircle(self.radius, x, y, data)
        i = random.randint(0,len(data.color) - 1)
        fill = data.color[i]
        canvas.create_oval(x0, y0, x1, y1, fill=fill, outline = fill, width = 0)

    def onTimerFired(self, data):
        dt = 0.01
        self.dx += data.splashR * math.sin(-self.theta) * data.omegaSplash * dt
        self.dy += data.splashR * math.cos(-self.theta) * data.omegaSplash * dt
        self.radius += 0.02

